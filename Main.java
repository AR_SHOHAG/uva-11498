import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int K, N, M, X, Y, i;
		K=in.nextInt();

	    while(K != 0){
	        if(K>0 && K<=1000){
	        	N=in.nextInt();
                M=in.nextInt();

                for(i=1; i<=K; ++i){
	                X=in.nextInt();
	                Y=in.nextInt();

	                if(X==N || Y==M)
	                	System.out.println("divisa");
	                else if(X>N && Y>M)
	                	System.out.println("NE");
	                else if(X<N && Y>M)
	                	System.out.println("NO");
	                else if(X<N && Y<M)
	                	System.out.println("SO");
	                else if(X>N && Y<M)
	                	System.out.println("SE");
	            }
	        }
	        N=0;
	        M=0;
	        K=in.nextInt();
	    }
	}
}
